import cssVars from "css-vars-ponyfill";

cssVars({
  onlyLegacy: false,
  onComplete: function(cssText, styleNodes, cssVariables, benchmark) {
    console.log(cssText);
    document.querySelector("body").innerHTML = cssText;
  }
});
