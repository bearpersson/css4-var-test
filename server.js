const Bundler = require("parcel-bundler");
const app = require("express")();
var proxy = require("express-http-proxy");

const file = "index.html";
const options = {};

const bundler = new Bundler(file, options);
app.use("/assets", proxy("http://localhost:8111"));
app.use(bundler.middleware());

app.listen(8080);
