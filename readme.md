# Beskrivning

Kör "npm install"
Starta med - "npm run start"

Se till att "content-server" körs på port 8111

Öppna sedan http://localhost:8080

Testa sedan att ändra färgvariablen i "content-server/public/src/color.css" till något annat och ladda om sidan (http://localhost:8080)
